<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php if (!is_home()): ?>
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 wow fadeInDownBig">
                    <div class="space4"></div>
                    <i class="fa fa-envelope"> </i>&nbsp;&nbsp;<a
                        href="mailto:servicioalestudiante@iberoamericana.edu.co">servicioalestudiante@iberoamericana.edu.co</a>

                    <hr/>
                    <p>Institucion de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación
                        Nacional</p>

                </div>
                <div class="col-md-2 wow fadeInDownBig">
                    <h3>Bogotá</h3>
                    <p>Calle 67 N 5-27.</p>
                    <p>Servicio al Estudiante:<BR/><i class="fa fa-phone"> </i>&nbsp;&nbsp;<a href="tel:15897550">(1)5897550</a><BR/>
                        Lunes a viernes de 8am a 8pm</p>
                    <p>Conmutador:<BR/><a href="tel:13489292"><i class="fa fa-phone"> </i>&nbsp;&nbsp;(1)3489292</a>
                        <BR/>Lunes a viernes de 8am a 6pm y <BR/>sábados de 8am a 1pm</p>

                </div>
                <div class="col-md-2 wow fadeInDownBig">
                    <h3>Neiva</h3>
                    <p>Calle 14 N 8B-65.</p>
                    <p><i class="fa fa-phone"> </i>&nbsp;&nbsp;<a href="tel:88758103">(8)8758103</a><br/><i
                            class="fa fa-phone"> </i>&nbsp;&nbsp;<a href="tel:88759009">(8)8759009</a></p>
                    <p>Colombia, Sur América</p>

                </div>
                <div class="col-md-3 col-md-offset-2 wow fadeInDownBig social-footer">
                    <a href="http://iberoamericana.edu.co/index.php" target="_self"><img class="img-responsive"
                                                                                         src="<?php echo bloginfo('template_url') ?>/images/logo_header.png"
                                                                                         alt="Iberoamericana"></a><br/>
                    <p><a href="https://www.facebook.com/LaIberoamericana" title="Facebook" target="_self"><i
                                class="fa fa-facebook-official"></i></a>
                        <a href="https://twitter.com/IberoamericanaU" title="Twitter" target="_self"><i
                                class="fa fa-twitter"></i></a>
                        <a href="http://instagram.com/laiberou" title="Instagram" target="_self"><i
                                class="fa fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UC68bjC1Ed6ei66zyCICalJA" title="Youtube"
                           target="_self"><i class="fa fa-youtube"></i></a>
                        <a href="https://plus.google.com/u/4/103193074968981010475/posts" title="Google+"
                           target="_self"><i class="fa fa-google-plus-official"></i></a>
                        <a href="https://www.linkedin.com/company/corporaci%C3%B3n-universitaria-iberoamericana"
                           title="LinkedIn" target="_self"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
        </div>
        <div class="space4"></div>
    </section>
<?php endif; ?>

<!-- Modal HTML embedded directly into document -->
<div id="habeas-modal" style="display:none;">
    <p>Autorizo a la CORPORACIÓN UNIVERSITARIA IBEROAMERICANA para el uso de la información aquí indicada, así como para
        que me envíen información relevante, en ejercicio de sus funciones como institución de Educación Superior; y en
        cumplimiento de la Ley de protección de datos Personales, la información contenida en este formato y en todos
        sus anexos es confidencial y/o privilegiada.</p>
    <p>En el evento de requerir actualizar su información en la base de datos institucional favor dirigirse al correo
        institucional <a href="mailto:habeasdata@iberoamericana.edu.co">habeasdata@iberoamericana.edu.co</a></p>
    <a href="#" rel="modal:close">Cerrar</a>
</div>

<!--Javascript -->
<script src="<?php echo bloginfo('template_url') ?>/assets/js/jquery.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/js/jquery-scrollto.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/bootbox/bootbox.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/jqueryvalidate/jquery.validate.min.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/js/jquery.modal.min.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/jquerymask/mask.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/tooltipsy/tooltipsy.source.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/js/wow.js"></script>
<script src="<?php echo bloginfo('template_url') ?>/assets/js/script.js"></script>
<script>
    // Resuelve la altura de los campos llenados del form
    jQuery('.form-control').each(function () {
        jQuery(this).on({
            focus: function () {
                jQuery(this).addClass('input-filled');
            },
            focusout: function () {
                if (jQuery(this).val() === '') {
                    jQuery(this).removeClass('input-filled');
                }
            }
        });
    });
</script>
<script>
    //Inicializa el motor de animación
    wow = new WOW(
        {
            animateClass: 'animated',
            offset: 100,
            callback: function (box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        }
    );
    wow.init();
</script>
<script type="text/javascript">
    // crea el botón de volver arriba
    var amountScrolled = 300;
    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('.back-to-top').fadeIn('slow');
        } else {
            $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top, a.simple-back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });
</script>
<script type="text/javascript">
    // esconde el CTA
    var ctaScrolled = 1400;
    $(window).scroll(function () {
        if ($(window).scrollTop() < ctaScrolled) {
            $('a.sticky').fadeIn('slow');
        } else {
            $('a.sticky').fadeOut('slow');
        }
    });
</script>

<?php wp_footer(); ?>
</body>
</html>
