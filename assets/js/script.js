$(document).ready(function(){
    var ajaxurl = $("#ajaxurl").val();
    var template_url = $("#template_url").val();

    $(".movil").mask(
        "00-0000000000"
//        {placeholder:"Celular o Teléfono fijo"}
    );
    
    $('#movil').tooltipsy({
        offset: [-10, 0],
        css: {
            'padding': '10px',
            'max-width': '200px',
            'color': '#fff',
            'background-color': '#222',
            '-moz-box-shadow': 'inset 0 0 10px #000',
            '-webkit-box-shadow': 'inset 0 0 10px #000',
            'box-shadow': 'inset 0 0 10px #000',
            'text-shadow': '0 0 3px #000',
            '-moz-border-radius': '10px',
            '-webkit-border-radius': '10px',
            'border-radius': '10px',
        }
    });
    

    $("#form_landing").validate({
        rules: {
            nombres: {
                required: true
            },
            apellidos: {
                required: true
            },
            documento: {
                required: true,
                digits: true,
                minlength: 7,
                maxlength: 15,
            },
            movil: {
                required: true
            },
            email: {
                required: true
            },
            programas: {
                required: true
            },
            habeas: {
                required: true
            }
        },
        messages: {
            nombres: {
                required: "Este campo es obligatorio"
            },
            apellidos: {
                required: "Este campo es obligatorio"
            },
            movil: {
                required: "Este campo es obligatorio"
            },
            email: {
                required: "Este campo es obligatorio"
            },
            programas: {
                required: "Este campo es obligatorio"
            },
            documento: {
                required: "Este campo es obligatorio",
                minlength: "Mínimo 7 caracteres",
                maxlength: "Máximo 15 caracteres",
                digits: "Solo se permiten números"
            },
            habeas: {
                required: "Debes aceptar las condiciones"
            }
        }
    });
    $("#ajaxButton").click(function(){
        if ($('#form_landing').validate().form() === false){
            return false;
        }
        
        var CEDetalleMedio = $("#CEDetalleMedio").val();
        var programa = $("#programas").val();
        
        $("#loading").fadeIn()
        var form = $("#form_landing").serializeArray();

        
        var finalForm = {};
        $.each(form, function (i, myForm) {
            finalForm[myForm["name"]] = myForm["value"];
        });
        
        jQuery.ajax({
            type:"POST",
            url: ajaxurl,
            
            data: {
                action: "WS_Connect",
                datos: finalForm,
            },
            success:function(data){
                var response = $.parseJSON(data);
                console.log(data)
                if(response.code == "0"){
                    if(CEDetalleMedio == "GOOADBUS"){
                        if(programa == 2648){
                            window.location= template_url+"/administracionYFinanzas.php";
                        }else if(programa == 13568){
                            window.location= template_url+"/psicologia.php";
                        }else if(programa == 102940){
                            window.location= template_url+"/marketingNegociosInternacionales.php";
                        }else if(programa == 14018){
                            window.location= template_url+"/pedagogiaInfantilPresencial.php";
                        }else if(programa == 90963){
                            window.location= template_url+"/pedagogiaInfantilVirtual.php";
                        }else if(programa == 2053){
                            window.location= template_url+"/educacionEspecialPresencial.php";
                        }else if(programa == 91112){
                            window.location= template_url+"/educacionEspecialVirtual.php";
                        }else if(programa == 102818){
                            window.location= template_url+"/contaduriaPublicaPresencial.php";
                        }else if(programa == 105216){
                            window.location= template_url+"/contaduriaPublicaVirtual.php";
                        }else{
                            window.location= template_url+"/generico.php";
                        }
                    }else{
                        if(programa == 2648){
                            window.location= "http://adminfinanzasgraciasi.estudiaenlaibero.co/";
                        }else if(programa == 13568){
                            window.location= "http://psicologiapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 102940){
                            window.location= "http://mktynegociosgracias.estudiaenlaibero.co/";
                        }else if(programa == 14018){
                            window.location= "http://pedagogiapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 90963){
                            window.location= "http://pedagogiavirtualgracias.estudiaenlaibero.co/";
                        }else if(programa == 2053){
                            window.location= "http://eduespecialpresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 91112){
                            window.location= "http://eduespecialvirtualgracias.estudiaenlaibero.co/";
                        }else if(programa == 102818){
                            window.location= "http://contaduriapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 105216){
                            window.location= "http://contaduriavritualgracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 105719){
                            window.location= "http://psicologiavirtualgracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 103179){
                            window.location= "http://adminlogisticagracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 2055){
                            window.location= "http://fonoaudiologiagracias.estudiaenlaibero.co/";
                        }
                        //window.location= template_url+"/gracias.php";
                    }
                }
                if(response.code == "null"){
                    bootbox.alert("Ha ocurrido un error de conexión, por favor intente mas tarde.", function(){
                        location.reload();
                    });
                }
                if(response.code == "52" ){
                    bootbox.alert("Campos inválidos: <br>"+response.description);
                }
                if(response.code == "2" ){
                    bootbox.alert("Ya te encuentras registrado en nuestro sistema. Próximamente un asesor se pondrá en contacto contígo muy pronto para ayudarte a tomar la mejor decisión para tu futuro");
                }
                
                $("#loading").fadeOut()
                console.log(data)
            },
            error: function(errorThrown){
                $("#loading").fadeOut()
                console.log(errorThrown) 
            } 
        });
    });
});

$(document).ready(function(){
    var ajaxurl = $("#ajaxurl").val();
    var template_url = $("#template_url").val();

    $(".movil").mask(
        "00-0000000000"
//        {placeholder:"Celular o Teléfono fijo"}
    );
    
    $('#movil').tooltipsy({
        offset: [-10, 0],
        css: {
            'padding': '10px',
            'max-width': '200px',
            'color': '#fff',
            'background-color': '#222',
            '-moz-box-shadow': 'inset 0 0 10px #000',
            '-webkit-box-shadow': 'inset 0 0 10px #000',
            'box-shadow': 'inset 0 0 10px #000',
            'text-shadow': '0 0 3px #000',
            '-moz-border-radius': '10px',
            '-webkit-border-radius': '10px',
            'border-radius': '10px',
        }
    });
    

    $("#form_landing_secundario").validate({
        rules: {
            nombres: {
                required: true
            },
            apellidos: {
                required: true
            },
            documento: {
                required: true,
                digits: true,
                minlength: 7,
                maxlength: 15,
            },
            movil: {
                required: true
            },
            email: {
                required: true
            },
            programas: {
                required: true
            },
            habeas: {
                required: true
            }
        },
        messages: {
            nombres: {
                required: "Este campo es obligatorio"
            },
            apellidos: {
                required: "Este campo es obligatorio"
            },
            movil: {
                required: "Este campo es obligatorio"
            },
            email: {
                required: "Este campo es obligatorio"
            },
            programas: {
                required: "Este campo es obligatorio"
            },
            documento: {
                required: "Este campo es obligatorio",
                minlength: "Mínimo 7 caracteres",
                maxlength: "Máximo 15 caracteres",
                digits: "Solo se permiten números"
            },
            habeas: {
                required: "Debes aceptar las condiciones"
            }
        }
    });
    $("#ajaxButton_secundario").click(function(){
        if ($('#form_landing_secundario').validate().form() === false){
            return false;
        }
        
        var CEDetalleMedio = $("#CEDetalleMedio").val();
        var programa = $("#programas").val();
        
        $("#loading").fadeIn()
        var form = $("#form_landing_secundario").serializeArray();

        
        var finalForm = {};
        $.each(form, function (i, myForm) {
            finalForm[myForm["name"]] = myForm["value"];
        });
        
        jQuery.ajax({
            type:"POST",
            url: ajaxurl,
            
            data: {
                action: "WS_Connect",
                datos: finalForm,
            },
            success:function(data){ 
                var response = $.parseJSON(data);
                console.log(data)
                if(response.code == "0"){
                    if(CEDetalleMedio == "GOOADBUS"){
                        if(programa == 2648){
                            window.location= template_url+"formulario/graciasAdministracionYFinanzas.php";
                        }else if(programa == 13568){
                            window.location= template_url+"formulario/graciasPsicologia.php";
                        }else if(programa == 102940){
                            window.location= template_url+"/marketingNegociosInternacionales.php";
                        }else if(programa == 14018){
                            window.location= template_url+"formulario/graciasPedagogiaInfantilPresencial.php";
                        }else if(programa == 90963){
                            window.location= template_url+"formulario/graciasPedagogiaInfantilVirtual.php";
                        }else if(programa == 2053){
                            window.location= template_url+"formulario/graciasEducacionEspecialPresencial.php";
                        }else if(programa == 91112){
                            window.location= template_url+"formulario/graciasEducacionEspecialVirtual.php";
                        }else if(programa == 102818){
                            window.location= template_url+"formulario/graciasContaduriaPublicaPresencial.php";
                        }else if(programa == 105216){
                            window.location= template_url+"formulario/graciasContaduriaPublicaVirtual.php";
                        }else{
                            window.location= template_url+"/gracias.php";
                        }
                    }else{
                        if(programa == 2648){
                            window.location= "http://adminfinanzasgraciasi.estudiaenlaibero.co/";
                        }else if(programa == 13568){
                            window.location= "http://psicologiapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 102940){
                            window.location= "http://mktynegociosgracias.estudiaenlaibero.co/";
                        }else if(programa == 14018){
                            window.location= "http://pedagogiapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 90963){
                            window.location= "http://pedagogiavirtualgracias.estudiaenlaibero.co/";
                        }else if(programa == 2053){
                            window.location= "http://eduespecialpresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 91112){
                            window.location= "http://eduespecialvirtualgracias.estudiaenlaibero.co/";
                        }else if(programa == 102818){
                            window.location= "http://contaduriapresencialgracias.estudiaenlaibero.co/";
                        }else if(programa == 105216){
                            window.location= "http://contaduriavritualgracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 105719){
                            window.location= "http://psicologiavirtualgracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 103179){
                            window.location= "http://adminlogisticagracias.estudiaenlaibero.co/";
                        }
                        else if(programa == 2055){
                            window.location= "http://fonoaudiologiagracias.estudiaenlaibero.co/";
                        }
                        //window.location= template_url+"/generico.php";
                    }
                }
                if(response.code == "null"){
                    bootbox.alert("Ha ocurrido un error de conexión, por favor intente mas tarde.", function(){
                        location.reload();
                    });
                }
                if(response.code == "52" ){
                    bootbox.alert("Campos inválidos: <br>"+response.description);
                }
                if(response.code == "2" ){
                    bootbox.alert("Ya te encuentras registrado en nuestro sistema. Próximamente un asesor se pondrá en contacto contígo muy pronto para ayudarte a tomar la mejor decisión para tu futuro");
                }
                
                $("#loading").fadeOut()
                console.log(data)
            },
            error: function(errorThrown){
                $("#loading").fadeOut()
                console.log(errorThrown) 
            } 
        });
    });
});