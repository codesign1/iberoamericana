<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<style>
    body{
        background-color: #781600
    }
    
    .mensaje{
        color: #ffffff;
        padding-top: 10%;
        font-size: 18pt;
        text-align: center;
        
    }
    
</style>

<section>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mensaje">
                    CORPORACIÓN IBEROAMERICANA<br>
                    
                    <a href="<?php echo "http://iberoamericana.edu.co" ?>">Ir a Iberoamericana</a>
                </div>
            </div>
        </div>
    </section>

	
<?php get_footer(); ?>
