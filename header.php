<?php
include("landing/cookies.php");
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
        
        <link href="<?php echo bloginfo( 'template_url' ) ?>/assets/css/libs/bootstrap.min.css" rel="stylesheet">
  	<!-- Animación CSS -->
  	<link rel="stylesheet" href="<?php echo bloginfo( 'template_url' ) ?>/assets/css/libs/animate.css">
    <link rel="stylesheet" href="<?php echo bloginfo( 'template_url' ) ?>/assets/css/jquery.modal.min.css">
  	<!-- Custom CSS -->
  	<link rel="stylesheet" href="<?php echo bloginfo( 'template_url' ) ?>/assets/css/styles.css">
  	<!-- Fuentes -->
  	<link href="<?php echo bloginfo( 'template_url' ) ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,300" rel="stylesheet">
        
        <style>
/*		.wow:first-child {
			visibility: hidden;
		}*/
	</style>
        
        
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <input type="hidden" id="template_url" value="<?php echo bloginfo( 'template_url' ); ?>">
    <div id="loading">
        <img src="<?php echo bloginfo( 'template_url' ) ?>/images/loading.gif">
    </div>
    
<div id="home" style="display:none"></div>
    <a href="#" class="back-to-top"></a>
    <section class="header">
    	<div class="container">
        	<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 wow fadeInLeftBig">
            	<a href="http://iberoamericana.edu.co/index.php" target="_self"><img class="img-responsive" src="<?php echo bloginfo( 'template_url' ) ?>/images/logo_header.png" alt="Iberoamericana"></a>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 social-header wow fadeInRightBig">
            	<p><a href="https://www.facebook.com/LaIberoamericana" title="Facebook" target="_self"><i class="fa fa-facebook-official"></i></a>
                <a href="https://twitter.com/IberoamericanaU" title="Twitter" target="_self"><i class="fa fa-twitter"></i></a>
                <a href="http://instagram.com/laiberou" title="Instagram" target="_self"><i class="fa fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UC68bjC1Ed6ei66zyCICalJA" title="Youtube" target="_self"><i class="fa fa-youtube"></i></a>
                <a href="https://plus.google.com/u/4/103193074968981010475/posts" title="Google+" target="_self"><i class="fa fa-google-plus-official"></i></a>
                <a href="https://www.linkedin.com/company/corporaci%C3%B3n-universitaria-iberoamericana" title="LinkedIn" target="_self"><i class="fa fa-linkedin"></i></a>
                <a class=" btn btn-default pse" href="https://www.psepagos.co/PSEHostingUI/ShowTicketOffice.aspx?ID=3439" title="PSE" target="_self" ><img src="<?php echo bloginfo( 'template_url' ) ?>/images/pse_normal.png" width="30" height="30" alt="PSE"></a></p>
                <p class="phone-header"><i class="fa fa-phone">&nbsp;&nbsp;</i><a href="tel:13489292">(1)348 9292</a></p>
             </div>
        </div>
    </section>
    
<?php if(!is_home()): ?>
    <span class="hide-movil"><a id="sticky" class="btn sticky btn-danger" href="#" onclick="$('#formulario').ScrollTo({duration: 1000});"><p>¿Quieres<br/>recibir<br/>información<br/>sin costo?</p><p class="footer"><i class="fa fa-chevron-right"> </i>&nbsp;Click aquí</p></a></span>
<?php endif; ?>