<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post();?>
    <?php if(of_get_option("zona2_fondo")){ ?>
        <section class="title" style="background-image:url(<?php echo of_get_option("zona2_fondo") ?>)">
    <?php }else{ ?>
        <section class="title" style="background-image:url(<?php echo bloginfo( 'template_url' ) ?>/images/title_background_2.jpg)">
    <?php } ?>
    <div class="container">
        <div class="col-md-6 left wow fadeInUp">
                <div class="space4"></div>
                    <?php 
                    $tipo_programa = get_field( "tipo_programa" );
                    if( $tipo_programa ) { ?> 
                        <div class="badge"> <?php echo $tipo_programa; ?></div>
                    <?php } ?>
                
                <h1><?php the_title() ?></h1>
            <?php 
            $descripcion = get_field( "descripcion" );
            if( $descripcion ) { ?> 
                <?php echo $descripcion; ?>
            <?php } ?>
                
            <BR/>
        </div>
        <div class="col-md-6 right wow fadeInLeftBig">
            <?php if(of_get_option("zona2_imagen1")){ ?>
                <img class="img-responsive" src="<?php echo of_get_option("zona2_imagen1") ?>">
            <?php }else{ ?>
                <img class="img-responsive" src="<?php echo bloginfo( 'template_url' ) ?>/images/chica.png">
            <?php } ?>
        </div>
    </div>
</section>
<section class="movil-cta">
    <div class="container">
        <div class="col-md-12 left wow fadeInUp">
            <h1>¿Quieres recibir información sin costo? <a class="movil-cta" href="#" onclick="$('#formulario').ScrollTo({duration: 1000});">Click Aquí</a></h1>

        </div>
    </div>
</section>

<section class="curso">
    <div class="container">
        <div class="space3"></div>
        <div class="col-md-5 left wow fadeInUp">
            <?php the_content(); ?>
        </div>
        <div class="col-md-6 col-md-offset-1 right wow fadeIn">
            <div class="embed-responsive embed-responsive-16by9">
                <?php 
                $video_youtube = get_field("video_youtube" );
                if( $video_youtube ) { ?> 
                    <?php echo $video_youtube; ?>
                <?php } ?>
            </div>
            <div class="space3"></div>
        </div>
    </div>
</section>

<?php if(of_get_option("zona4_fondo")){ ?>
    <section class="formulario2" id="formulario2" style="background-image:url(<?php echo of_get_option("zona4_fondo") ?>)">
<?php }else{ ?>
    <section class="formulario2" id="formulario2" style="background-image:url(<?php echo bloginfo( 'template_url' ) ?>/images/formbackground.jpg)">
<?php } ?>
    <?php include("landing/landing.php"); ?>
</section>

<section class="beneficios">
        <div class="container">
            <div class="row">
                <div class="space3"></div>
                <div class="col-md-12 left wow fadeInUp">
                    <?php 
                    $zona5_titulo = of_get_option("zona5_titulo" );
                    if( $zona5_titulo ) { ?> 
                        <?php echo $zona5_titulo; ?>
                    <?php }else{ ?>
                        <h1>¿Porqué elegir la Ibero?</h1>
                        <h3>Algunos factores de los que te beneficiarás:</h3><br/>
                    <?php } ?>
                    <div class="space2"></div>
                </div>
            </div>
            <div class="row">
<!--            --><?php //for($i = 1; $i < 5; $i++): ?>
<!--                <div class="col-md-3 left wow fadeInUp beneficio">-->
<!--                    --><?php //if( of_get_option("zona5_item{$i}_icono") ) { ?>
<!--                        <i class="fa --><?php //echo of_get_option("zona5_item{$i}_icono") ?><!-- fa-4x"></i>-->
<!--                    --><?php //} ?>
<!---->
<!--                    --><?php //if( of_get_option("zona5_item{$i}_titulo") ) { ?>
<!--                        <h2>--><?php //echo of_get_option("zona5_item{$i}_titulo") ?><!--</h2>-->
<!--                    --><?php //} ?>
<!---->
<!--                    --><?php //if( of_get_option("zona5_item{$i}_descripcion") ) { ?>
<!--                        <p>--><?php //echo of_get_option("zona5_item{$i}_descripcion") ?><!--</p>-->
<!--                    --><?php //} ?>
<!--                </div>-->
<!--            --><?php //endfor; ?>
                
               <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-road fa-4x"></i>
                    <h2>Vanguardia</h2>
                    <p>Institución de Educación Superior pionera en Colombia en el trabajo por la inclusión y la diversidad.</p>
                </div>
                <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-trophy fa-4x"></i>
                    <h2>Reconocimiento</h2>
                    <p>Egresados con reconocimiento en el ámbito nacional e internacional, preparados para ser líderes en diversos ámbitos. Tenemos presencia en más de 100 municipios en Colombia.</p>
                </div>
                <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-bookmark fa-4x"></i>
                    <h2>Flexibilidad</h2>
                    <p>Puedes acceder a formación por ciclos (Técnico, tecnológico y profesional).</p>
                </div>
            </div>
            <div class="space3"></div>
            <div class="row">

                <!--<?php for($i = 5; $i < 8; $i++): ?>
                    <div class="col-md-3 left wow fadeInUp beneficio">
                        <?php if( of_get_option("zona5_item{$i}_icono") ) { ?>
                            <i class="fa <?php echo of_get_option("zona5_item{$i}_icono") ?> fa-4x"></i>
                        <?php } ?>

                        <?php if( of_get_option("zona5_item{$i}_titulo") ) { ?>
                            <h2><?php echo of_get_option("zona5_item{$i}_titulo") ?></h2>
                        <?php } ?>

                        <?php if( of_get_option("zona5_item{$i}_descripcion") ) { ?>
                            <p><?php echo of_get_option("zona5_item{$i}_descripcion") ?></p>
                        <?php } ?>
                    </div>
                <?php endfor; ?>-->
                
                <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-line-chart fa-4x"></i>
                    <h2>Proyección</h2>
                    <p>Contamos con planes de fácil acceso para doble titulación y acceso al aprendizaje de una segunda lengua.</p>
                </div>
                <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-check-square-o fa-4x"></i>
                    <h2>Beneficios</h2>
                    <p>Becas hasta del 50% fáciles de conseguir y facilidades de pago.</p>
                </div>
                <div class="col-md-3 left wow fadeInUp">
                    <i class="fa fa-desktop fa-4x"></i>
                    <h2>Tecnología</h2>
                    <p>Contamos con una de las mejores plataformas tecnológicas para programas virtuales.</p>
                </div>
                <div class="col-md-3 left wow fadeInRightBig">
                    <?php if( of_get_option("zona5_imagen1") ) { ?>
                        <img class="img-responsive" src="<?php echo of_get_option("zona5_imagen1") ?>" >
                    <?php }else{ ?>
                        <img class="img-responsive" src="<?php echo bloginfo( 'template_url' ) ?>/images/ok.jpg" >
                    <?php } ?>
                    
                </div>	
            </div>
        </div>    
    </section>


<?php if(of_get_option("zona2_fondo")){ ?>
        <section class="title" style="background-image:url(<?php echo of_get_option("zona2_fondo") ?>)">
    <?php }else{ ?>
        <section class="title" style="background-image:url(<?php echo bloginfo( 'template_url' ) ?>/images/title_background_2.jpg)">
    <?php } ?>
    <div class="space3"></div>
    <div class="container">
        <div class="col-md-8 wow fadeInLeftBig">
            <h1>¡Juntos, logramos más!</h1>
        </div>
        <div class="col-md-4 wow fadeInRightBig">
            <p class="cita">
                "La Ibero me permite desarrollarme en lo personal y en lo profesional"
            </p>
            <div>
                <p class="autor">
                    Liceth Gómez - Estudiante Ibero
                </p>
            </div>
        </div>
    </div>
</section>


<section class="formulario" id="formulario">
    <?php include("landing/landing_secundaria.php"); ?>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
