<?php
class Cookies {
    private $caducidad;
    
    function __construct() {
        $this->caducidad = time() + 28800;
    }
    
    public function setCookie($nombre, $valor) {
        setcookie($nombre, $valor, $this->caducidad, "/"); 
    }
    
    public function getCookie($name){
        if(isset($_COOKIE[$name])){
            return $_COOKIE[$name];
        }else{
            return false;
        }
    }
}
$cookies = new Cookies();

if(isset($_GET["URLReferrer"])){
    $URLReferrer = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['URLReferrer']);
    $nombre = "URLReferrer";
    $valor = $URLReferrer;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["CEDetalleFuente"])){
    $CEDetalleFuente = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['CEDetalleFuente']);
    $nombre = "CEDetalleFuente";
    $valor = $CEDetalleFuente;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["CEDetalleMedio"])){
    $CEDetalleMedio = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['CEDetalleMedio']);
    $nombre = "CEDetalleMedio";
    $valor = $CEDetalleMedio;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["CECampania"])){
    
    $CECampania = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['CECampania']);
    $nombre = "CECampania";
    $valor = $CECampania;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["IDTipoDocumento"])){
    $IDTipoDocumento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['IDTipoDocumento']);
    $nombre = "IDTipoDocumento";
    $valor = $IDTipoDocumento;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Telefono"])){
    $Telefono = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Telefono']);
    $nombre = "Telefono";
    $valor = $Telefono;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["IDPais"])){
    $IDPais = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['IDPais']);
    $nombre = "IDPais";
    $valor = $IDPais;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["IDDepartamento"])){
    $IDDepartamento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['IDDepartamento']);
    $nombre = "IDDepartamento";
    $valor = $IDDepartamento;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["IDCiudad"])){
    $IDCiudad = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['IDCiudad']);
    $nombre = "IDCiudad";
    $valor = $IDCiudad;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Direccion"])){
    $Direccion = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Direccion']);
    $nombre = "Direccion";
    $valor = $Direccion;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Codigo_Postal"])){
    $Codigo_Postal = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Codigo_Postal']);
    $nombre = "Codigo_Postal";
    $valor = $Codigo_Postal;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Sexo"])){
    $Sexo = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Sexo']);
    $nombre = "Sexo";
    $valor = $Sexo;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["FechaNacimiento"])){
    $FechaNacimiento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['FechaNacimiento']);
    $nombre = "FechaNacimiento";
    $valor = $FechaNacimiento;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Observaciones"])){
    $Observaciones = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Observaciones']);
    $nombre = "Observaciones";
    $valor = $Observaciones;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Profesion"])){
    $Profesion = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Profesion']);
    $nombre = "Profesion";
    $valor = $Profesion;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Sector"])){
    $Sector = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Sector']);
    $nombre = "Sector";
    $valor = $Sector;
    $cookies->setCookie($nombre, $valor);
}

if(isset($_GET["Edad"])){
    $Edad = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['Edad']);
    $nombre = "Edad";
    $valor = $Edad;
    $cookies->setCookie($nombre, $valor);
}