<?php

$cookie = new Cookies();

if ($_COOKIE["URLReferrer"]) {
    $URLReferrer = $_COOKIE["URLReferrer"];
} elseif (isset($_GET["URLReferrer"])) {
    $URLReferrer = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["URLReferrer"]);
} else {
    $URLReferrer = "";
}

if ($_COOKIE["CEDetalleFuente"]) {
    $CEDetalleFuente = $_COOKIE["CEDetalleFuente"];
} elseif (isset($_GET["CEDetalleFuente"])) {
    $CEDetalleFuente = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["CEDetalleFuente"]);
} else {
    $CEDetalleFuente = "IBERO";
}

if ($_COOKIE["CEDetalleMedio"]) {
    $CEDetalleMedio = $_COOKIE["CEDetalleMedio"];
} elseif (isset($_GET["CEDetalleMedio"])) {
    $CEDetalleMedio = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["CEDetalleMedio"]);
} else {
    $CEDetalleMedio = "";
}

if ($_COOKIE["CECampania"]) {
    $CECampania = $_COOKIE["CECampania"];
} elseif (isset($_GET["CECampania"])) {
    $CECampania = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["CECampania"]);
} else {
    $CECampania = "";
}

if ($_COOKIE["IDTipoDocumento"]) {
    $IDTipoDocumento = $_COOKIE["IDTipoDocumento"];
} elseif (isset($_GET["IDTipoDocumento"])) {
    $IDTipoDocumento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["IDTipoDocumento"]);
} else {
    $IDTipoDocumento = "";
}

if ($_COOKIE["Telefono"]) {
    $Telefono = $_COOKIE["Telefono"];
} elseif (isset($_GET["Telefono"])) {
    $Telefono = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Telefono"]);
} else {
    $Telefono = "";
}

if ($_COOKIE["IDPais"]) {
    $IDPais = $_COOKIE["IDPais"];
} elseif (isset($_GET["IDPais"])) {
    $IDPais = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["IDPais"]);
} else {
    $IDPais = "CO";
}

if ($_COOKIE["IDDepartamento"]) {
    $IDDepartamento = $_COOKIE["IDDepartamento"];
} elseif (isset($_GET["IDDepartamento"])) {
    $IDDepartamento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["IDDepartamento"]);
} else {
    $IDDepartamento = "";
}

if ($_COOKIE["IDCiudad"]) {
    $IDCiudad = $_COOKIE["IDCiudad"];
} elseif (isset($_GET["IDCiudad"])) {
    $IDCiudad = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["IDCiudad"]);
} else {
    $IDCiudad = "";
}

if ($_COOKIE["Direccion"]) {
    $Direccion = $_COOKIE["Direccion"];
} elseif (isset($_GET["Direccion"])) {
    $Direccion = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Direccion"]);
} else {
    $Direccion = "";
}

if ($_COOKIE["Codigo_Postal"]) {
    $Codigo_Postal = $_COOKIE["Codigo_Postal"];
} elseif (isset($_GET["Codigo_Postal"])) {
    $Codigo_Postal = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Codigo_Postal"]);
} else {
    $Codigo_Postal = "";
}

if ($_COOKIE["Sexo"]) {
    $Sexo = $_COOKIE["Sexo"];
} elseif (isset($_GET["Sexo"])) {
    $Sexo = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Sexo"]);
} else {
    $Sexo = "";
}

if ($_COOKIE["FechaNacimiento"]) {
    $FechaNacimiento = $_COOKIE["FechaNacimiento"];
} elseif (isset($_GET["FechaNacimiento"])) {
    $FechaNacimiento = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["FechaNacimiento"]);
} else {
    $FechaNacimiento = "";
}

if ($_COOKIE["Observaciones"]) {
    $Observaciones = $_COOKIE["Observaciones"];
} elseif (isset($_GET["Observaciones"])) {
    $Observaciones = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Observaciones"]);
} else {
    $Observaciones = "";
}

if ($_COOKIE["Profesion"]) {
    $Profesion = $_COOKIE["Profesion"];
} elseif (isset($_GET["Profesion"])) {
    $Profesion = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Profesion"]);
} else {
    $Profesion = "";
}

if ($_COOKIE["Sector"]) {
    $Sector = $_COOKIE["Sector"];
} elseif (isset($_GET["Sector"])) {
    $Sector = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Sector"]);
} else {
    $Sector = "";
}

if ($_COOKIE["Edad"]) {
    $Edad = $_COOKIE["Edad"];
} elseif (isset($_GET["Edad"])) {
    $Edad = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET["Edad"]);
} else {
    $Edad = "";
}


$parent = wp_get_post_parent_id(get_the_ID());
$postParent = get_post($parent);
?>

<div class="space5"></div>
<div class="container">

    <div class="space2"></div>
    <div class="col-md-5 col-md-offset-7 form-block contact-form-block blanco">
        <div class="col-md-1">
            <h2 class="wow fadeInUpBig"><i class="fa fa-chevron-right"> </i>&nbsp;</h2>
        </div>
        <div class="col-md-11">
            <h2 class="wow fadeInUpBig">Infórmate gratis y recibe nuestro plan de estudios</h2>
        </div>

        <div class="form-block-container">
            <form class="form_landing" id="form_landing" method="post">
                <input id="base_url" type="hidden" value="http://iberoamericana.edu.co/">
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="form_nombre" type="text" name="nombres" class="form-control contact-name" required>
                        <label>Nombres</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" name="apellidos" class="form-control contact-name" required>
                        <label>Apellidos</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" name="Documento" class="form-control contact-name" required>
                        <label>Documento</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="tel" name="movil" class="form-control contact-name movil" id="movil" required
                               title="Recuerda registrar el código de país, código de area y número de teléfono fijo o celular">
                        <label>Celular</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="email" name="email" class="form-control contact-email" required>
                        <label>Email</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <label id="habeas-label"><input name="habeas" required type="checkbox">Acepto los <a href="#habeas-modal" rel="modal:open">Términos y Condiciones</a></label>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php $snies = get_field("snies"); ?>
                        <input type="hidden" value="<?php echo $snies ?>" id="programas" name="programas">
                        <input type="hidden" value="<?php echo admin_url('admin-ajax.php'); ?>" id="ajaxurl">
                        <button class="btn btn-gfort wave-effect" type="button" id="ajaxButton">Enviar</button>
                    </div>
                </div>


                <input type="hidden" id="snies_hidden" value="<?php echo $snies ?>">
                <input type="hidden" id="title_hidden" value="<?php the_title(); ?>">
                <input type="hidden" name="URLReferrer" value="<?php echo $URLReferrer ?>">
                <input type="hidden" name="CEDetalleFuente" value="<?php echo $CEDetalleFuente ?>">
                <input type="hidden" id="CEDetalleMedio" name="CEDetalleMedio" value="<?php echo $CEDetalleMedio ?>">
                <input type="hidden" name="CECampania" value="<?php echo $CECampania ?>">
                <input type="hidden" name="IDTipoDocumento" value="<?php echo $IDTipoDocumento ?>">
                <!--<input type="hidden" name="Documento" value="<?php //echo $Documento ?>">-->
                <input type="hidden" name="Telefono" value="<?php echo $Telefono ?>">
                <input type="hidden" name="IDPais" value="<?php echo $IDPais ?>">
                <input type="hidden" name="IDDepartamento" value="<?php echo $IDDepartamento ?>">
                <input type="hidden" name="IDCiudad" value="<?php echo $IDCiudad ?>">
                <input type="hidden" name="Direccion" value="<?php echo $Direccion ?>">
                <input type="hidden" name="Sexo" value="<?php echo $Sexo ?>">
                <input type="hidden" name="Observaciones" value="<?php echo $Observaciones ?>">
                <input type="hidden" name="Profesion" value="<?php echo $Profesion ?>">
                <input type="hidden" name="Sector" value="<?php echo $Sector ?>">
                <input type="hidden" name="Edad" value="<?php echo $Edad ?>">

            </form>
        </div>

    </div>
</div>
<div class="space5"></div>