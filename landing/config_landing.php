<?php

class Config_Landing {
//    private $url = "https://iberoamericana.zoomcrm.co/home.php/api/main";
//    private $urlWsdlLanding = "https://iberoamericana.zoomcrm.co/home.php/api/main?wsdl";
    private $url = "https://iberoamericana.zoomcrm.co/home.php/api/main";
    private $urlWsdlLanding = "https://iberoamericana.zoomcrm.co/home.php/api/main?wsdl";
    private $privatekey = "090c20aea787902231b73423b9486164ac83ab13";
    private $publicKey = "e7edbe97fb097e3a03e4be0cdf13fcc3f7e54968";
    private $username = "landing_ibero";
    private $password = "iberoamericana.2016!";

    public $auth;
    public $key;
    public $hash;
    public $URLReferrer;
    public $CEDetalleFuente;
    public $IDCampania;
    public $IDProducto;
    public $IDTipoDocumento;
    public $Documento;
    public $Nombre;
    public $Apellidos;
    public $Telefono;
    public $Movil;
    public $e_mail;
    public $IDPais;
    public $IDDepartamento;
    public $IDCiudad;
    public $Direccion;
    public $Codigo_Postal;
    public $Sexo;
    public $FechaNacimiento;
    public $Observaciones;
    public $Profesion;
    public $Sector;
    public $Edad;


    public function ws_connection($datos) {

        date_default_timezone_set('America/Bogota');
        $fecha = date('YmdHis');
        $data2 = $fecha . "-" . $this->username . sha1($this->password);
        $hash = $this->encrypt($data2, $this->privatekey);

        $URLReferrer = filter_var($datos["URLReferrer"], FILTER_SANITIZE_STRING);
        $CEDetalleFuente = filter_var($datos["CEDetalleFuente"], FILTER_SANITIZE_STRING);
        $CEDetalleMedio = filter_var($datos["CEDetalleMedio"], FILTER_SANITIZE_STRING);
        $nombres = filter_var($datos["nombres"], FILTER_SANITIZE_STRING);
        $apellidos = filter_var($datos["apellidos"], FILTER_SANITIZE_STRING);
        $movil = filter_var($datos["movil"], FILTER_SANITIZE_STRING);
        $email = filter_var($datos["email"], FILTER_SANITIZE_STRING);
        $CECampania = filter_var($datos["CECampania"], FILTER_SANITIZE_STRING);
        $IDTipoDocumento = filter_var($datos["IDTipoDocumento"], FILTER_SANITIZE_STRING);
        $Documento = filter_var($datos["Documento"], FILTER_SANITIZE_STRING);
        $Telefono = filter_var($datos["Telefono"], FILTER_SANITIZE_STRING);
        $IDPais = filter_var($datos["IDPais"], FILTER_SANITIZE_STRING);
        $IDDepartamento = filter_var($datos["IDDepartamento"], FILTER_SANITIZE_STRING);
        $IDCiudad = filter_var($datos["IDCiudad"], FILTER_SANITIZE_STRING);
        $Direccion = filter_var($datos["Direccion"], FILTER_SANITIZE_STRING);
        $Codigo_Postal = filter_var($datos["Codigo_Postal"], FILTER_SANITIZE_STRING);
        $Sexo = filter_var($datos["Sexo"], FILTER_SANITIZE_STRING);
        $FechaNacimiento = filter_var($datos["FechaNacimiento"], FILTER_SANITIZE_STRING);
        $Observaciones = filter_var($datos["Observaciones"], FILTER_SANITIZE_STRING);
        $Profesion = filter_var($datos["Profesion"], FILTER_SANITIZE_STRING);
        $Sector = filter_var($datos["Sector"], FILTER_SANITIZE_STRING);
        $Edad = filter_var($datos["Edad"], FILTER_SANITIZE_STRING);
        $programasString = filter_var($datos["programas"], FILTER_SANITIZE_STRING);

        $save2Db = array(
            "'{$URLReferrer}'",
            "'{$CEDetalleFuente}'",
            "'{$CEDetalleMedio}'",
            "'{$nombres}'",
            "'{$apellidos}'",
            "'{$movil}'",
            "'{$email}'",
            "'{$CECampania}'",
            "'{$IDTipoDocumento}'",
            "'{$Documento}'",
            "'{$Telefono}'",
            "'{$IDPais}'",
            "'{$IDDepartamento}'",
            "'{$IDCiudad}'",
            "'{$Direccion}'",
            "'{$Codigo_Postal}'",
            "'{$Sexo}'",
            "'{$FechaNacimiento}'",
            "'{$Observaciones}'",
            "'$Profesion'",
            "'$Sector'",
            "'{$Edad}'",
            "'{$programasString}'"

        );

//        $db = new Database();

        $oportunidad = new Oportunidad();
        $oportunidad->key = $this->publicKey;
        $oportunidad->auth->key = $this->publicKey;
        $oportunidad->auth->hash = $hash;
        $oportunidad->URLReferrer = $URLReferrer;
        $oportunidad->CEDetalleFuente = $CEDetalleFuente;
        $oportunidad->CEDetalleMedio = $CEDetalleMedio;
        $oportunidad->CECampania = $CECampania;
        $oportunidad->IDProducto = $programasString;
        $oportunidad->IDTipoDocumento = $IDTipoDocumento;
        $oportunidad->Documento = $Documento;
        $oportunidad->Nombre = $nombres;
        $oportunidad->Apellidos = $apellidos;
        $oportunidad->Telefono = $Telefono;
        $oportunidad->Movil = $movil;
        $oportunidad->e_mail = $email;
        $oportunidad->IDPais = $IDPais;
        $oportunidad->IDDepartamento = $IDDepartamento;
        $oportunidad->IDCiudad = $IDCiudad;
        $oportunidad->Direccion = $Direccion;
        $oportunidad->Codigo_Postal = $Codigo_Postal;
        $oportunidad->Sexo = $Sexo;
        $oportunidad->FechaNacimiento = $FechaNacimiento;
        $oportunidad->Observaciones = $Observaciones;
        $oportunidad->Profesion = $Profesion;
        $oportunidad->Sector = $Sector;
        $oportunidad->Edad = $Edad;


        try {
            include 'nusoap/lib/nusoap.php';
            $client = new SoapClient($this->urlWsdlLanding);
//            $xml = $this->xmlCreateOportunityLead($oportunidad);
            $soapResponse = $client->createOportunityLead($oportunidad);
            $response = array(
                "code" => $soapResponse->Code,
                "description" => $soapResponse->Description,
            );

//            if ($codigo !== 0) {
//                $db->insertLeads($save2Db);
//            }

            return json_encode($response, JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            error_log($e);
        }
    }

    private function encrypt($data, $key) {
        $salt = mcrypt_create_iv(128, MCRYPT_DEV_URANDOM);
        list ($cipherKey, $macKey, $iv) = $this->getKeys($salt, $key);

        $data = $this->pad($data);

        $enc = mcrypt_encrypt(MCRYPT_BLOWFISH, $cipherKey, $data, MCRYPT_MODE_CBC, $iv);

        $mac = hash_hmac('sha512', $enc, $macKey, true);
        return base64_encode($salt . $enc . $mac);
    }

    private function pad($data) {
        $length = mcrypt_get_block_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $padAmount = $length - strlen($data) % $length;
        if ($padAmount == 0) {
            $padAmount = $length;
        }
        return $data . str_repeat(chr($padAmount), $padAmount);
    }

    private function getKeys($salt, $key) {
        $ivSize = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $keySize = mcrypt_get_key_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $length = 2 * $keySize + $ivSize;

        $key = $this->pbkdf2('sha512', $key, $salt, 100, $length);

        $cipherKey = substr($key, 0, $keySize);
        $macKey = substr($key, $keySize, $keySize);
        $iv = substr($key, 2 * $keySize);
        return array($cipherKey, $macKey, $iv);
    }

    private function pbkdf2($algo, $key, $salt, $rounds, $length) {
        $size = strlen(hash($algo, '', true));
        $len = ceil($length / $size);
        $result = '';
        for ($i = 1; $i <= $len; $i++) {
            $tmp = hash_hmac($algo, $salt . pack('N', $i), $key, true);
            $res = $tmp;
            for ($j = 1; $j < $rounds; $j++) {
                $tmp = hash_hmac($algo, $tmp, $key, true);
                $res ^= $tmp;
            }
            $result .= $res;
        }
        return substr($result, 0, $length);
    }


    private function xmlCreateOportunityLead($data) {
        $xml = "<CreateOportunityLeadRequest xsi:type='main:CreateOportunityLeadRequest' xmlns:main='http://landing.zoomcrm.co/home.php/api/main'>
                 <!--You may enter the following 24 items in any order-->
                <key xsi:type='xsd:string'>{$data->key}</key>
                <auth xsi:type='main:Auth'>
                  <key xsi:type='xsd:string'>{$data->key}</key>
                  <hash xsi:type='xsd:string'>{$data->hash}</hash>
                </auth>
                <CEDetalleFuente xsi:type='xsd:string'>{$data->CEDetalleFuente}</CEDetalleFuente>
                <CEDetalleMedio xsi:type='xsd:string'>{$data->CEDetalleMedio}</CEDetalleMedio>
                <URLReferrer xsi:type='xsd:string'>{$data->URLReferrer}</URLReferrer>
                <CECampania xsi:type='xsd:string'>{$data->CECampania}</CECampania>
                <IDProducto xsi:type='xsd:string'>{$data->IDProducto}</IDProducto>
                <IDTipoDocumento xsi:type='xsd:string'>{$data->IDTipoDocumento}</IDTipoDocumento>
                <Documento xsi:type='xsd:string'>{$data->Documento}</Documento>
                <Nombre xsi:type='xsd:string'>{$data->Nombre}</Nombre>
                <Apellidos xsi:type='xsd:string'>{$data->Apellidos}</Apellidos>
                <Telefono xsi:type='xsd:string'>{$data->Telefono}</Telefono>
                <Movil xsi:type='xsd:string'>{$data->Movil}</Movil>
                <e_mail xsi:type='xsd:string'>{$data->e_mail}</e_mail>
                <IDPais xsi:type='xsd:string'>{$data->IDPais}</IDPais>
                <IDDepartamento xsi:type='xsd:string'>{$data->IDDepartamento}</IDDepartamento>
                <IDCiudad xsi:type='xsd:string'>{$data->IDCiudad}</IDCiudad>
                <Direccion xsi:type='xsd:string'>{$data->Direccion}</Direccion>
                <Codigo_Postal xsi:type='xsd:string'>{$data->Codigo_Postal}</Codigo_Postal>
                <Sexo xsi:type='xsd:string'>{$data->Sexo}</Sexo>
                <FechaNacimiento xsi:type='xsd:string'>{$data->FechaNacimiento}</FechaNacimiento>
                <Edad xsi:type='xsd:string'>{$data->Edad}</Edad>
                <Observaciones xsi:type='xsd:string'>{$data->Observaciones}</Observaciones>
                <Profesion xsi:type='xsd:string'>{$data->Profesion}</Profesion>
                <Sector xsi:type='xsd:string'>{$data->Sector}</Sector>
              </CreateOportunityLeadRequest>";
        return $xml;
    }

}


class Oportunidad {
    public $auth;
    public $key;
    public $hash;
    public $URLReferrer;
    public $CEDetalleFuente;
    public $CEDetalleMedio;
    public $CECampania;
    public $IDProducto;
    public $IDTipoDocumento;
    public $Documento;
    public $Nombre;
    public $Apellidos;
    public $Telefono;
    public $Movil;
    public $e_mail;
    public $IDPais;
    public $IDDepartamento;
    public $IDCiudad;
    public $Direccion;
    public $Codigo_Postal;
    public $Sexo;
    public $FechaNacimiento;
    public $Observaciones;
    public $Profesion;
    public $Sector;
    public $Edad;
}
